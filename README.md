# Custom kernel module to remove options from outgoing IPv4 packets

This is a proof of concept out-of-tree kernel module that strips any IPv4 options from packets that are sent out through a given network interface (or all interfaces).

It can be useful to make IP traffic able to pass through routers that are configured to drop packets with any IP options (e.g. due to STIG rules such as [V-90937](https://www.stigviewer.com/stig/juniper_router_rtr/2019-09-27/finding/V-90937) or [V-217001](https://www.stigviewer.com/stig/cisco_ios_xe_router_rtr/2021-03-26/finding/V-217001)).

## Usage

To build the module, simply run `make` in this directory. To build for a specific kernel version, use `make KVER=<version>`.

To load the module, run `insmod ipv4_strip_opts.ko`. If you want the IP options to be stripped only on a given outgoing interface, load the module by running `insmod ipv4_strip_opts.ko interface=<interface name>`.

To unload the module, run `rmmod ipv4_strip_opts`.
