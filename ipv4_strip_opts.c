// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: Copyright (c) 2024 Red Hat, Inc.

#include <linux/init.h>
#include <linux/module.h>
#include <linux/netfilter_ipv4.h>
#include <linux/version.h>

#include <net/ip.h>
#include <net/net_namespace.h>

#ifdef RHEL_MAJOR
#if RHEL_MAJOR < 9
static void iph_set_totlen(struct iphdr *iph, unsigned int len)
{
	iph->tot_len = len <= IP_MAX_MTU ? htons(len) : 0;
}
#endif /* RHEL_MAJOR < 10 */
#if RHEL_MAJOR < 8
#define OLD_HOOKS
#endif /* RHEL_MAJOR < 8 */
#endif /* RHEL_MAJOR */

static char *interface = NULL;
module_param(interface, charp, 0660);

#ifdef OLD_HOOKS
static unsigned int mod_ipv4_strip_opts_transform(const struct nf_hook_ops *ops,
						  struct sk_buff *skb,
						  const struct net_device *in,
						  const struct net_device *out,
						  const struct nf_hook_state *state)
#else
static unsigned int mod_ipv4_strip_opts_transform(void *priv, struct sk_buff *skb,
						  const struct nf_hook_state *state)
#endif /* OLD_HOOKS */
{
	int ret, hlen, delta;
	struct iphdr *iph;
	struct ip_options *opt = &IPCB(skb)->opt;

	if (interface && strcmp(interface, state->out->name) != 0)
		return NF_ACCEPT;

	iph = ip_hdr(skb);
	hlen = iph->ihl * 4;
	if (hlen <= sizeof(struct iphdr))
		return NF_ACCEPT;

	ret = skb_cow(skb, skb_headroom(skb));
	if (WARN_ON(ret))
		return NF_DROP;

	delta = hlen - sizeof(struct iphdr);
	memmove((unsigned char *)iph + delta, iph, sizeof(struct iphdr));
	skb_pull(skb, delta);
	skb_reset_network_header(skb);
	iph = ip_hdr(skb);

	opt->optlen = 0;
	opt->srr = 0;
	opt->rr = 0;
	opt->ts = 0;
	opt->router_alert = 0;
	opt->cipso = 0;
	opt->is_strictroute = 0;
	opt->srr_is_hit = 0;
	opt->is_changed = 0;
	opt->rr_needaddr = 0;
	opt->ts_needtime = 0;
	opt->ts_needaddr = 0;
	opt->is_changed = 1;

	iph->ihl = sizeof(struct iphdr) / 4;
	iph_set_totlen(iph, skb->len);
	ip_send_check(iph);

	return NF_ACCEPT;
}

static
#ifndef OLD_HOOKS
    const
#endif
    struct nf_hook_ops mod_ipv4_strip_opts_nf_ops[] = {
	{
		.hook =		mod_ipv4_strip_opts_transform,
		.pf =		NFPROTO_IPV4,
		.hooknum =	NF_INET_LOCAL_OUT,
		.priority =	NF_IP_PRI_SELINUX_FIRST,
	},
	{
		.hook =		mod_ipv4_strip_opts_transform,
		.pf =		NFPROTO_IPV4,
		.hooknum =	NF_INET_FORWARD,
		.priority =	NF_IP_PRI_SELINUX_FIRST,
	},
};

#ifdef OLD_HOOKS
static int __init mod_ipv4_strip_opts_init(void)
{
	return nf_register_hooks(mod_ipv4_strip_opts_nf_ops,
				 ARRAY_SIZE(mod_ipv4_strip_opts_nf_ops));
}

static void __exit mod_ipv4_strip_opts_exit(void)
{
	nf_unregister_hooks(mod_ipv4_strip_opts_nf_ops,
			    ARRAY_SIZE(mod_ipv4_strip_opts_nf_ops));
}
#else
static int __net_init mod_ipv4_strip_opts_nf_register(struct net *net)
{
	return nf_register_net_hooks(net, mod_ipv4_strip_opts_nf_ops,
				     ARRAY_SIZE(mod_ipv4_strip_opts_nf_ops));
}

static void __net_exit mod_ipv4_strip_opts_nf_unregister(struct net *net)
{
	nf_unregister_net_hooks(net, mod_ipv4_strip_opts_nf_ops,
				ARRAY_SIZE(mod_ipv4_strip_opts_nf_ops));
}

static struct pernet_operations mod_ipv4_strip_opts_net_ops = {
	.init = mod_ipv4_strip_opts_nf_register,
	.exit = mod_ipv4_strip_opts_nf_unregister,
};

static int __init mod_ipv4_strip_opts_init(void)
{
	return register_pernet_subsys(&mod_ipv4_strip_opts_net_ops);
}

static void __exit mod_ipv4_strip_opts_exit(void)
{
	unregister_pernet_subsys(&mod_ipv4_strip_opts_net_ops);
}
#endif /* OLD_HOOKS */

module_init(mod_ipv4_strip_opts_init);
module_exit(mod_ipv4_strip_opts_exit);
MODULE_LICENSE("GPL");
