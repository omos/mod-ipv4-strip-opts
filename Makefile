KVER ?= $(shell uname -r)
KDIR ?= /lib/modules/$(KVER)/build

obj-m := ipv4_strip_opts.o

.PHONY: all clean

all:
	$(MAKE) -C $(KDIR) M=$(PWD)

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
